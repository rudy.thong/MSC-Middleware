<?php

function Redirect($logger, $request, $key, $env) {

  $basket = $request['BASKET'];
  $transIdMerchant = $request['TRANSIDMERCHANT'];
  $storeId = $request['STOREID'];
  $amount = $request['AMOUNT'];
  $url = $request['URL'];
  $name = $request['CNAME'];
  $email = $request['CEMAIL'];
  $officePhone = $request['CWPHONE'];
  $homePhone = $request['CHPHONE'];
  $mobilePhone = $request['CMPHONE'];
  $address = $request['CADDRESS'];
  $zipCode = $request['CZIPCODE'];
  $birthdate = $request['BIRTHDATE'];
  $city = $request['CCITY'];
  $state = $request['CSTATE'];
  $country = $request['CCOUNTRY'];
  $shippingAddress = $request['SADDRESS'];
  $shippingZipCode = $request['SZIPCODE'];
  $shippingCity = $request['SCITY'];
  $shippingState = $request['SSTATE'];
  $shippingCountry = $request['SCOUNTRY'];
  $words = $request['WORDS'];

  $key = $keys[$storeId];

  $logger->info('Checking Words ');
  $incWords = sha1($amount . $keys['mscSharedkey'] . $transIdMerchant);
  if($words != $incWords) {
      return 'Invalid Words ';
  }
  $logger->info('Words is valid');

  $logger->info('Sending Verify ');
  $verifyURL = $keys['verifyURL'];
  $verifyData = array('TRANSIDMERCHANT' => $transIdMerchant,
                      'AMOUNT' => $amount,
                      'STOREID' => $storeId);
  $logger->info(var_export($verifyData, true));
  $handle = curl_init($verifyURL);
  curl_setopt($handle, CURLOPT_POST, true);
  curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  $verifyResult = curl_exec($handle);
  $logger->info($verifyResult);
  if($verifyResult != 'Continue') {
      return '<h1>Verify Failed!</h1>';
  }

  $logger->info('Composing parameter to OCO');
  $redirectURL = $env['redirectURL'];
  $ocoMallId = $keys['mallId'];
  $ocoTransIdMerchant = $storeId . $transIdMerchant;
  $requestDateTime = date('YmdHis');
  $currency = '360';

  $logger->info('Generating new words for OCO');
  $newWords = sha1($amount . $ocoMallId . $keys['ocoSharedKey'] . $ocoTransIdMerchant);

  $logger->info('Redirect to OCO');
  $pWrite = '
      <div style="display:none">
        <form id="ocoForm" method="post" action="'.$redirectURL.'">
          <input type="text" name="MALLID" value="'.$ocoMallId.'"></input>
          <input type="text" name="CHAINMERCHANT" value="'.$keys['ocoChainMerchant'].'"></input>
          <input type="text" name="AMOUNT" value="'.$amount.'"></input>
          <input type="text" name="PURCHASEAMOUNT" value="'.$amount.'"></input>
          <input type="text" name="TRANSIDMERCHANT" value="'.$ocoTransIdMerchant.'"></input>
          <input type="text" name="WORDS" value="'.$newWords.'"></input>
          <input type="text" name="REQUESTDATETIME" value="'.$requestDateTime.'"></input>
          <input type="text" name="CURRENCY" value="'.$currency.'"></input>
          <input type="text" name="PURCHASECURRENCY" value="'.$currency.'"></input>
          <input type="text" name="SESSIONID" value="'.$storeId.'"></input>
          <input type="text" name="NAME" value="'.substr($name, 0, 50).'"></input>
          <input type="text" name="EMAIL" value="'.substr($email, 0, 100).'"></input>
          <input type="text" name="BASKET" value="'.$basket.'"></input>
          <input type="text" name="SHIPPING_ADDRESS" value="'.substr($shippingAddress, 0, 100).'"></input>
          <input type="text" name="SHIPPING_CITY" value="'.$shippingCity.'"></input>
          <input type="text" name="SHIPPING_STATE" value="'.$shippingState.'"></input>
          <input type="text" name="SHIPPING_COUNTRY" value="ID"></input>
          <input type="text" name="SHIPPING_ZIPCODE" value="'.$shippingZipCode.'"></input>
          <input type="submit" name="button">Kirim</input>
        </form>
      </div>
  ';
  $logger->info(var_export($pWrite, true));
  return $pWrite;
}

function Notify($logger, $request, $key) {

  $amount = $request['AMOUNT'];
  $transIdMerchant = $request['TRANSIDMERCHANT'];
  $words = $request['WORDS'];
  $statusType = $request['STATUSTYPE'];
  $responseCode = $request['RESPONSECODE'];
  $approvalCode = $request['APPROVALCODE'];
  $resultMsg = $request['RESULTMSG'];
  $paymentChannel = $request['PAYMENTCHANNEL'];
  $paymentCode = $request['PAYMENTCODE'];
  $sessionId = $request['SESSIONID'];
  $bank = $request['BANK'];
  $mcn = $request['MCN'];
  $paymentDateTime = $request['PAYMENTDATETIME'];
  $verifyId = $request['VERIFYID'];
  $verifyScore = $request['VERIFYSCORE'];
  $verifyStatus = $request['VERIFYSTATUS'];

  $keys = $key[$sessionId];

  $logger->info('Checking Words ');
  $incWords = sha1($amount . $keys['ocoSharedKey'] . $transIdMerchant . $resultMsg . $verifyStatus);
  if($words != $incWords) {
      return 'Invalid Words ';
  }
  $logger->info('Words is valid');

  $logger->info('Composing parameter to MSC Merchants');
  $mscTransIdMerchant = $transIdMerchant;
  $pos = strpos($transIdMerchant, $sessionId);
  if ($pos !== false) {
      $mscTransIdMerchant = substr_replace($transIdMerchant, '', $pos, strlen($sessionId));
  }
  $mscResult = ucfirst($resultMsg);
  $mscPType = paymentChannelConverter($paymentchannel);

  $logger->info('No need to generate Words in Notify :(');

  $logger->info('Sending Notify');
  $notifyURL = $keys['notifyURL'];
  $notifyData = array('TRANSIDMERCHANT' => $mscTransIdMerchant,
                      'AMOUNT' => $amount,
                      'RESULT' => $mscResult,
                      'PTYPE' => $mscPType);
  $logger->info(var_export($notifyData, true));
  $handle = curl_init($notifyURL);
  $fieldsString = http_build_query($notifyData);
  curl_setopt($handle, CURLOPT_POST, true);
  curl_setopt($handle, CURLOPT_POSTFIELDS, $fieldsString);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  $notifyResult = curl_exec($handle);
  $logger->info($notifyResult);
  if($notifyResult == 'Continue') {
      return 'Continue';
  } else {
      return 'Stop';
  }

  function Callback($logger, $request, $key) {

    $amount = $request['AMOUNT'];
    $transIdMerchant = $request['TRANSIDMERCHANT'];
    $words = $request['WORDS'];
    $statusCode = $request['STATUSCODE'];
    $paymentChannel = $request['PAYMENTCHANNEL'];
    $sessionId = $request['SESSIONID'];
    $paymentCode = $request['PAYMENTCODE'];

    $keys = $key[$sessionId];

    $logger->info('Checking Words ');
    $incWords = sha1($amount . $keys['ocoSharedKey'] . $transIdMerchant . $statusCode);
    if($words != $incWords) {
        return 'Invalid Words ';
    }
    $logger->info('Words is valid');

    $logger->info('Composing parameter to MSC Merchants');
    $mscTransIdMerchant = $transIdMerchant;
    $pos = strpos($transIdMerchant, $sessionId);
    if ($pos !== false) {
        $mscTransIdMerchant = substr_replace($transIdMerchant, '', $pos, strlen($sessionId));
    }
    $mscResult = ucfirst($resultMsg);
    $mscStatusCode = substr($statusCode, -2);
    $mscResult = 'Failed';
    if($statusCode == '0000') {
      $mscResult = 'Success';
    } else if($statusCode == '5511') {
      $mscResult = 'Pending';
    }
    $mscTransDate = date('Y-m-d');
    $mscPType = paymentChannelConverter($paymentchannel);
    $mscExtraInfo = '';

    $logger->info('Redirect back to Merchant');
    $callbackURL = $keys['callbackURL'];
    $pWrite = '
        <div style="display:none">
          <form id="ocoForm" method="post" action="'.$callbackURL.'">
            <input type="text" name="TRANSIDMERCHANT" value="'.$mscTransIdMerchant.'"></input>
            <input type="text" name="STATUSCODE" value="'.$mscStatusCode.'"></input>
            <input type="text" name="TRANSDATE" value="'.$mscTransDate.'"></input>
            <input type="text" name="PTYPE" value="'.$mscPType.'"></input>
            <input type="text" name="AMOUNT" value="'.$amount.'"></input>
            <input type="text" name="RESULT" value="'.$mscResult.'"></input>
            <input type="text" name="EXTRAINFO" value=""></input>
            <input type="submit" name="button">Kirim</input>
          </form>
        </div>
    ';
    $logger->info(var_export($pWrite, true));
  }
}
