<?php
function getData() {

  return [

      'OCO-dev' => [
          'redirectURL' => 'http://staging.doku.com/Suite/Receive'
      ],

      'OCO-prod' => [
          'redirectURL' => 'https://pay.doku.com/Suite/Receive'
      ],

      'merchants-dev' => [
          '10566449' => [
              'mscSharedkey' => 'E5a9h2S6n5P4',
              'mallId' => '2053',
              'ocoSharedKey' => '1706893147',
              'ocoChainMerchant' => 'NA',
              'verifyURL' => 'http://www.amacom3d.com/verifycontinue.php',
              'notifyURL' => 'http://www.amacom3d.com/verifycontinue.php'.
              'callbackURL' => 'http://posttestserver.com/post.php'
          ]
      ],

      'merchants-prod' => [
          '10566449' => [
              'mscSharedkey' => 'E5a9h2S6n5P4',
              'mallId' => '2053',
              'ocoSharedKey' => '1706893147',
              'ocoChainMerchant' => 'NA',
              'verifyURL' => 'http://www.amacom3d.com/verifycontinue.php',
              'notifyURL' => 'http://www.amacom3d.com/verifycontinue.php'.
              'callbackURL' => 'http://posttestserver.com/post.php'
          ]
      ]
  ];
}

function paymentChannelConverter($paymentchannel) {

  switch ($paymentchannel) {
      case 01:
        return 'CREDIT CARD';
        break;
      case 02:
        return 'Mandiriclickpay';  //Check
        break;
      case 04:
        return 'DOKU WALLET';
        break;
      case 05:
        return 'BANK TRANSFER';
        break;
      case 06:
        return 'Briepay';  //Check
        break;
      case 08:
        return 'Mandiri Bank Transfer';
        break;
      case 14:
        return 'ALFAMART'; 
        break;
      case 15:
        return 'CREDIT CARD';
        break;
      case 18:
        return 'BCA KlikPay';
        break;
      case 24:
        return 'BCA KlikPay';
        break;
  }
}
