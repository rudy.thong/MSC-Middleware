<?php

// DEVELOPMENT  =============================================================

$app->post('/redirect-dev', function ($request, $response, $args) {

    $this->logger->info("===== Incoming Redirect ======");

    $this->logger->info(var_export($request->getParams(), true));

    $storeId = $request->getParams()['STOREID'];

    $result = Redirect($this->logger, $request->getParams(), getData()['merchants-dev'], getData()['OCO-dev']);

    $this->logger->info("===== Ending Redirect ======");

    $response->getBody()->write($result);
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/notify-dev', function ($request, $response, $args) {

  $this->logger->info("===== Incoming Notify ======");

  $this->logger->info(var_export($request->getParams(), true));

  $result = Notify($this->logger, $request->getParams(), getData()['merchants-dev']);

  $this->logger->info("===== Ending Notify ======");

  // Render index view
  $response->getBody()->write($result);
  return $this->renderer->render($response, 'response.phtml', $args);

});

$app->post('/callback-dev', function ($request, $response, $args) {

    $this->logger->info("===== Incoming Callback ======");

    $this->logger->info(var_export($request->getParams(), true));

    $result = Callback($this->logger, $request->getParams(), getData()['merchants-dev']);

    $this->logger->info("===== Ending Callback ======");

    // Render index view
    $response->getBody()->write($result);
    return $this->renderer->render($response, 'index.phtml', $args);

});

// PRODUCTION  =============================================================

$app->post('/redirect', function ($request, $response, $args) {

    $this->logger->info("===== Incoming Redirect ======");

    $this->logger->info(var_export($request->getParams(), true));

    $storeId = $request->getParams()['STOREID'];

    $result = Redirect($this->logger, $request->getParams(), getData()['merchants-prod'], getData()['OCO-prod']);

    $this->logger->info("===== Ending Redirect ======");

    $response->getBody()->write($result);
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/notify', function ($request, $response, $args) {

  $this->logger->info("===== Incoming Notify ======");

  $this->logger->info(var_export($request->getParams(), true));

  $result = Notify($this->logger, $request->getParams(), getData()['merchants-prod']);

  $this->logger->info("===== Ending Notify ======");

  // Render index view
  $response->getBody()->write($result);
  return $this->renderer->render($response, 'response.phtml', $args);

});

$app->post('/callback', function ($request, $response, $args) {

    $this->logger->info("===== Incoming Callback ======");

    $this->logger->info(var_export($request->getParams(), true));

    $result = Callback($this->logger, $request->getParams(), getData()['merchants-prod']);

    $this->logger->info("===== Ending Callback ======");

    // Render index view
    $response->getBody()->write($result);
    return $this->renderer->render($response, 'index.phtml', $args);

});
